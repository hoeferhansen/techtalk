log = console.log;

class MyPromise {

    constructor(executor) {
        executor(this.resolve.bind(this), this.reject.bind(this))

        this.successCb = () => null;
        this.errorCb = () => null;
    }

    then(successCb) {
        const self = this

        return new MyPromise((resolve1, reject1) => {
            self.successCb = (...params) => {
                const res = successCb(...params);

                if (res instanceof MyPromise) {
                    res.then((...params2) => resolve1(...params2))
                } else {
                    resolve1(res);
                }
            }

        })
    }

    catch(errorCb) {
        this.errorCb = errorCb;
    }

    resolve(...params) {
        try {
            this.successCb(...params)
        } catch (e) {
            this.reject(e)
        }
    }

    reject(...params) {
        this.errorCb(...params)
    }

}


const pr = new MyPromise((resolve, reject) => {
    setTimeout(() => resolve('code'), 1000);
});

pr
    .then((x) => {
        log(x);
        return 'test'
    })
    .then((x) => {
        log(x)
        return new MyPromise((resolve, reject) => {
            setTimeout(() => resolve('deploy'), 1000);
        });
    })
    .then((x) => {
        log(x);
        return 'repeat'
    })
    .then(log)
