log = console.log;
class MyPromise {

    constructor(executor) {
        executor(this.resolve.bind(this), this.reject.bind(this))

        this.successCb = () => null;
        this.errorCb = () => null;
    }

    then(successCb) {
        const self = this

        return new MyPromise((resolve1, reject1) => {
            self.successCb = (...params) => {
                const res = successCb(...params);

                if (res instanceof MyPromise) {
                    res.then((...params2) => resolve1(...params2))
                } else {
                    resolve1(res);
                }
            }

        })
    }

    catch(errorCb) {
        this.errorCb = errorCb;
    }

    resolve(...params) {
        try {
            this.successCb(...params)
        } catch (e) {
            this.reject(e)
        }
    }

    reject(...params) {
        this.errorCb(...params)
    }

}

const waitAndReturn = (returnVal, seconds) => new MyPromise((resolve, reject) => {
    setTimeout(() => resolve(returnVal), seconds);
});
const logTap = (x) => {
    log(x);
    return x;
}
const logAndWait = (msg, time) => {
    return waitAndReturn(msg, time).then(logTap)
}

const code = logAndWait('code', 10);
const test = logAndWait('test', 10);
const deploy = logAndWait('deploy', 10);
const repeat = logAndWait('repeat', 10);



const pr = new MyPromise((resolve, reject) => {
    setTimeout(() => resolve('code'), 1000);
});

pr
    .then(code)
    .then(test)
    .then(deploy)
    .then(repeat)

