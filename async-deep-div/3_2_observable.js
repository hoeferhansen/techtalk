class MyObservable {

    constructor() {
        this.subscriber = [];
    }

    subscribe(cb) {
        this.subscriber.push(cb)
    }

    next(value) {
        this.subscriber.forEach(cb => {
            cb(value)
        });
    }
}

const mapTenTimes = x => x * 10

const obs = new MyObservable();
obs.subscribe(console.log);
obs.subscribe(console.log);

obs.next('code')
obs.next('test')
obs.next('deploy')

setTimeout(() => obs.next('bugfix'), 1000)

