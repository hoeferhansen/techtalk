const https = require("https");
const log = console.log;

const url = "https://jsonplaceholder.typicode.com/posts/1";

log('== json api request ==')
https.get(url, res => {

    res.setEncoding("utf8");
    let body = "";

    res.on("data", data => {
        body += data;
    });

    res.on("end", () => {
        body = JSON.parse(body);
        log(body);
    });
});

