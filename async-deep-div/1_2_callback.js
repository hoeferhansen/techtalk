const https = require("https");
const log = console.log;

const code = (cb) => {
    log('code');
    return cb();
}

const test = (cb) => {
    log('test');
    return cb();
}

const deploy = (cb) => {
    log('deploy');
    return cb();
}

const repeat = (cb) => {
    log('repeat');
    cb();
}


code(
    () => test(
        () => deploy(
            () => repeat(
                () => log('find the bug')
            )
        )
    )
)

