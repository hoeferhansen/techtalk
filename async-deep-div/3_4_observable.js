
const { from, of } = require("rxjs");
const { map, tap, delay, filter, bufferTime, concatMap } = require("rxjs/operators");

log = console.log

const customerWhishlist = [
    'login',
    'registration',
    'dashbaord',
    'chat',
    'search',
    'rocket',
    'responsiveness',
]

const mapToTicket = (title, owner) => ({
    title, owner, deadline: 'yesterday'
})
const team = [
    'Pascal', 'Laura', 'Simon', 'Johanna', 'Lukas', 'Dennis', 'Nick', 'Niko', 'Simon'
]
const getRandomTeamMember = () => team[Math.floor(Math.random() * team.length)];
const getRandomDelay = (min, max) => {
    return Math.random() * (max - min) + min;
}
const tasks = from(customerWhishlist).pipe(
    map(title => mapToTicket(title, getRandomTeamMember())),
    concatMap(i => of(i).pipe(delay(getRandomDelay(100, 3000)))),
    tap(t => log(`${t.owner} works on ${t.title}`)),
    bufferTime(3000),
    filter(tickets => tickets.length > 0),
    map(solvedTickets => solvedTickets.map(t => t.title).join(', ')),
    map(tickets => `🎉 release of: ${tickets}`),
)


tasks.subscribe(console.log);
