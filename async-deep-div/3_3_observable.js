const assignSimon = x => `Simon: ${x.replace('always', 'never')}`
const assignLaura = x => `Laura: ${x}`
const assignNiko = x => `Niko: ${x}`
class MyObservable {

    constructor() {
        this.subscriber = [];
        this.pipeFuncs = [];
    }

    subscribe(cb) {
        this.subscriber.push(cb)
    }

    next(value) {
        this.subscriber.forEach(cb => {
            cb(this.pipeFuncs.reduce((previousValue, currentValue) => currentValue(previousValue), value))
        });
    }

    pipe(...functions) {

        const newObs = new MyObservable()

        this.subscribe(val => newObs.next(val));
        newObs.pipeFuncs.push(...functions)

        return newObs
    }
}

const obs = new MyObservable();

// obs.subscribe(console.log);
obs.pipe(assignSimon).subscribe(console.log);
obs.pipe(assignLaura).subscribe(console.log);
obs.pipe(assignNiko).subscribe(console.log);

obs.next('code')
obs.next('test')
obs.next('deploy')

setTimeout(() => obs.next('there are always bugs'), 500)

