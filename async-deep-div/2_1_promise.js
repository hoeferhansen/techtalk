class MyPromise {

    constructor(executor) {
        executor(this.resolve.bind(this), this.reject.bind(this))

        this.successCb = () => null;
        this.errorCb = () => null;
    }

    then(successCb) {
        this.successCb = successCb
    }

    catch(errorCb) {
        this.errorCb = errorCb;
    }

    resolve(...params) {
        try {
            this.successCb(...params)
        } catch (e) {
            this.reject(e)
        }
    }

    reject(...params) {
        this.errorCb(...params)
    }
}


const pr = new MyPromise((resolve, reject) => {
    setTimeout(() => resolve('getting started'), 1000);
});

pr.then((x) => console.log(x))
    .then(console.log)

